FROM node:14.17.1-slim AS build 
WORKDIR /home/app
COPY . .
RUN npm install --global gatsby
RUN yarn
RUN yarn run build


FROM nginx
COPY --from=build /home/app/public /usr/share/nginx/html